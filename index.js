const Discord = require("discord.js");
const client = new Discord.Client();
const admin = require("firebase-admin");
const OneSignal = require("onesignal-node");

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: "https://fillydelphia-radio.firebaseio.com",
});

const onesig = new OneSignal.Client(process.env.OSAPPID, process.env.OSAPIKEY);

var db = admin.database();
var ref = db.ref("rest/fillydelphia-radio/now-playing");

let loggedIn = false;

ref.on("value", async (snapshot) => {
  if (client.channels) {
    client.user.setPresence({
      game: { name: `${snapshot.val().title} by ${snapshot.val().artist}` },
    });
    client.channels
      .get("451612225480818690")
      .setTopic(
        `ON AIR: **${snapshot.val().title}** by **${
          snapshot.val().artist
        }** | https://fillyradio.com/`,
        "SONG UPDATE PLS IGNORE"
      )
      .catch((e) => {
        console.log(e);
      });
    client.channels
      .get("540953370001670157")
      .setTopic(
        `${snapshot.val().listeners} listeners | ${
          snapshot.val().icename
            ? `DJ CURRENTLY LIVE! ${snapshot.val().icename}`
            : `No live DJ atm!`
        }`,
        "SONG UPDATE PLS IGNORE"
      )
      .catch((e) => {
        console.log(e);
      });

    if (snapshot.val().icename) {
      const embed = new Discord.RichEmbed();
      embed
        .setAuthor(
          "Currently Playing on Fillydelphia Radio:",
          "https://res.cloudinary.com/stormdragon-designs/image/upload/s--CcNNewBL--/c_scale,w_200/v1559905603/frlogonew_uh2pu9.png",
          "https://fillydelphiaradio.net/"
        )
        .setTitle(snapshot.val().icename)
        .setDescription("Live on Air! \nTune in now @ https://fillyradio.com/")
        .setURL("https://fillyradio.com/")
        .setColor("#82d7ff")
        .setThumbnail(snapshot.val().covers[256]);

      if (snapshot.val().history[1].icename != snapshot.val().icename) {
        client.channels
          .get("540953370001670157")
          .send(embed)
          .catch((e) => {
            console.log(e);
          });

        // onesignal
        const notification = {
          headings: {
            en: "DJ Live now on Fillydelphia Radio",
          },
          contents: {
            en: snapshot.val().icename,
          },
          url: "https://fillydelphiaradio.net/",
          ttl: 7200,
          chrome_web_badge: snapshot.val().covers[128],
          chrome_web_icon: snapshot.val().covers[256],
          included_segments: ["Subscribed Users"],
        };

        try {
          const response = await onesig.createNotification(notification);
          console.log(response.body.id);
        } catch (e) {
          if (e instanceof OneSignal.HTTPError) {
            // When status code of HTTP response is not 2xx, HTTPError is thrown.
            console.log(e.statusCode);
            console.log(e.body);
          }
        }
      }
    }
  }
});

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
  loggedIn = true;
});

client.on;

client.on("invalidated", () => {
  console.log("We got logged out. Restarting");
  process.exit(0);
});

client.on("error", (e) => {
  console.log("We encountered an error!. Restarting");
  console.log(e);
  process.exit(0);
});

client.on("warn", (i) => {
  console.log("Warning emmitted from Client()");
  console.log(i);
});

client.on("message", (msg) => {
  if (msg.content === "!np") {
    const embed = new Discord.RichEmbed();
    ref.once("value", function (snapshot) {
      const {
        title,
        artist,
        comment = "",
        isLive,
        icename = "",
        wwwpublisher = "none",
      } = snapshot.val();

      let store =
        wwwpublisher === "none" ? "" : `\nBuy song from ${wwwpublisher}`;

      comment.includes("Visit ")
        ? (store = `\nBuy song from ${comment.substring(6)}`)
        : null;

      const message = isLive
        ? `Live DJ: ${icename}\n${title}\nby ${artist}\nListen @ https://fillyradio.com/`
        : `${title}\nby ${artist}\nListen @ https://fillyradio.com/${store}`;

      embed
        .setTitle("Currently Playing on Fillydelphia Radio:")
        .setDescription(message)
        .setURL("https://fillyradio.com/")
        .setColor("#82d7ff")
        .setThumbnail(snapshot.val().covers[128]);

      msg.channel.send(embed).catch((e) => {
        console.log(e);
      });
    });
  } else if (msg.content === "!recent") {
    const embed = new Discord.RichEmbed();
    ref.once("value", function (snapshot) {
      const { history } = snapshot.val();

      let message = "";

      history.forEach((key, index) => {
        let time;
        if (key.on_air) {
          time = CalculateTimeSince(key.on_air);
        }
        const timemsg =
          index === 0
            ? `Currently Playing: `
            : `${time ? `${time} mins ago: ` : ""}`;

        message =
          message +
          `${timemsg}**${key.title}** *by* **${key.artist}**\n${
            index === 0 ? `\n` : ""
          }`;
      });

      embed
        .setTitle("Currently Playing on Fillydelphia Radio:")
        .setDescription(message)
        .setURL("https://fillyradio.com/")
        .setColor("#82d7ff")
        .setThumbnail(snapshot.val().covers[128]);

      msg.channel.send(embed).catch((e) => {
        console.log(e);
      });
    });
  } else if (
    (msg.content === "OPT-IN" || msg.content === "opt-in") &&
    msg.channel.name === "notification-request"
  ) {
    let role = msg.guild.roles.find(
      (role) => role.name === "Notification Squad"
    );

    let user = msg.member;
    user
      .addRole(role)
      .then(() => {
        console.log("success");
      })
      .catch((err) => console.log(err));
    msg.delete().catch((e) => {
      console.log(e);
    });
  } else if (
    (msg.content === "OPT-OUT" || msg.content === "opt-out") &&
    msg.channel.name === "notification-request"
  ) {
    let role = msg.guild.roles.find(
      (role) => role.name === "Notification Squad"
    );

    let user = msg.member;
    user
      .removeRole(role)
      .then(() => {
        console.log("success");
      })
      .catch((err) => console.log(err));
    msg.delete().catch((e) => {
      console.log(e);
    });
  }
});

client.on("guildMemberAdd", (member) => {
  const channel = member.guild.channels.find((ch) => ch.name === "general");
  const embed = new Discord.RichEmbed();

  embed
    .setColor("#82d7ff")
    .setURL("https://fillydelphiaradio.net/")
    .setDescription(
      "Welcome to the Fillydelphia Radio Discord!\nBe sure to check out our website: https://fillyradio.com/"
    )
    .setThumbnail(member.user.avatarURL)
    .setTitle(`Welcome, ${member.user.username}!`);

  channel.send(embed).catch((e) => {
    console.log(e);
  });
});

function CalculateTimeSince(songtime) {
  const timePlayed = new Date(songtime + " GMT+0000");
  const timeSince = Date.now() - new Date(timePlayed);
  const minutesSince = Math.round(timeSince / 60000);
  return minutesSince;
}

client.login(process.env.DISCORD_KEY).catch((e) => {
  console.log(e);
});
